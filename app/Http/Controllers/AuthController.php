<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth/login');
    }

    public function register()
    {
        return view('auth/register');
    }

    public function login_process(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'email:rfc,dns'],
            'password' => ['required', 'min:8'],
        ]);

        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {
            return redirect()->back()->with('error', 'Login failed, please check your credentials');
        }

        $user = User::where('email', $credentials['email'])->first();

        $token = $user->createToken(config('app.name'))->plainTextToken;
        $request->session()->put('LoginSession', $token);

        if ($user) {
            return redirect()->intended('dashboard')->with('success', 'Login success');
        }
    }

    public function register_process(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'max:24', 'regex:/^[a-zA-Z0-9]{1,24}$/'],
            'email' => ['required', 'email:rfc,dns', 'unique:users,email', 'max:100'],
            'username' => ['required', 'max:25', 'regex:/^\S*$/'],
            'address' => ['required'],
            'password' => [
                'required', 'confirmed', 'min:8', 'max:15', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/'
            ],
            'photo_profile' => ['nullable', 'image', 'mimes:jpeg,png,jpg', 'max:2048']
        ]);

        $registration_data = $request->except('photo_profile');
        $registration_data['password'] = Hash::make($registration_data['password']);
        $registration_data['photo_profile'] = ''; // set a default value

        $user = User::create($registration_data);

        if ($user) {
            if ($request->hasFile('photo_profile')) {
                $filename = 'pp_of_' . $user->id . '.' . $request->photo_profile->extension();
                $request->photo_profile->move(public_path('image'), $filename);
                $user->update(['photo_profile' => $filename]);
            }

            return redirect()->intended('login')->with('success', 'Registration success, please login');
        }

        return redirect()->back()->with('error', 'Registration failed, please try again');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->intended('login');
    }
}
