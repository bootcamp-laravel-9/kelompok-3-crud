<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function edit()
    {
        $user = Auth::user();
        return view('profile/profile-edit', compact('user'));
    }

    public function index(){
        $user = Auth::user();
        return view('profile/profile-index', compact('user'));
    }

    public function update_profile(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required', 'max:24', 'regex:/^[a-zA-Z0-9]{1,24}$/'],
            'email' => ['required', 'email:rfc,dns', 'max:100'],
            'username' => ['required', 'max:25', 'regex:/^\S*$/'],
            'address' => ['required', 'max:255'],
            'password' => [
                'nullable', 'confirmed', 'min:8', 'max:15', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/'
            ],
            'photo_profile' => ['nullable', 'image', 'mimes:jpeg,png,jpg', 'max:2048']
        ]);

        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->address = $request->address;

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        if ($request->hasFile('photo_profile')) {
            if (File::exists('image/' . $user->photo_profile)) {
                File::delete('image/' . $user->photo_profile);
            }

            $filename = 'pp_of_' . $user->id . '.' . $request->photo_profile->extension();
            $request->photo_profile->move(public_path('image'), $filename);
            $user->photo_profile = $filename;
        }

        $user->save();

        return redirect()->intended('dashboard')->with('message', 'Data Profile Telah Berhasil Di Update');
    }
}
