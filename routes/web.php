<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::namespace('App\Http\Controllers')->group(function () {
    Route::group(['middleware' => ['guest']], function () {
        Route::get('login', 'AuthController@login');
        Route::get('register', 'AuthController@register');
        Route::post('login', 'AuthController@login_process');
        Route::post('register', 'AuthController@register_process');
    });

    Route::group(['middleware' => ['isSessionValid']], function () {
        // Dashboard Route
        Route::get('dashboard', 'DashboardController@index');

        // Profile Route
        Route::get('profile-edit', 'UserController@edit');
        Route::get('profile-index', 'UserController@index');
        Route::put('profile-update/{id}', 'UserController@update_profile');
        // Logout Route
        Route::get('logout', 'AuthController@logout');
    });
});
